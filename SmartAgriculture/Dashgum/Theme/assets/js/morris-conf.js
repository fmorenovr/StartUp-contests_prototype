var Script = function () {

    //morris chart

    $(function () {
      // data stolen from http://howmanyleft.co.uk/vehicle/jaguar_'e'_type
      var tax_data = [
           {"period": "2011 Q3", "Temperatura": 28, "sorned": 25},
           {"period": "2011 Q2", "Temperatura": 25, "sorned": 29},
           {"period": "2011 Q1", "Temperatura": 26, "sorned": 18},
           {"period": "2010 Q4", "Temperatura": 23, "sorned": 31},
           {"period": "2009 Q4", "Temperatura": 24, "sorned": 27},
           {"period": "2008 Q4", "Temperatura": 29, "sorned": 25},
           {"period": "2007 Q4", "Temperatura": 22, "sorned": 23},
           {"period": "2006 Q4", "Temperatura": 27, "sorned": null},
           {"period": "2005 Q4", "Temperatura": 31, "sorned": null}
      ];
      Morris.Line({
        element: 'hero-graph',
        data: tax_data,
        xkey: 'period',
        ykeys: ['Temperatura', 'sorned'],
        labels: ['Temperatura', 'CO2'],
        lineColors:['#4ECDC4','#ed5565']
      });
/*
      Morris.Donut({
        element: 'hero-donut',
        data: [
          {label: 'Jam', value: 25 },
          {label: 'Frosted', value: 40 },
          {label: 'Custard', value: 25 },
          {label: 'Sugar', value: 10 }
        ],
          colors: ['#3498db', '#2980b9', '#34495e'],
        formatter: function (y) { return y + "%" }
      });
*/
      Morris.Area({
        element: 'hero-area',
        data: [
          {period: '2010 Q1', Temperatura: 26, Humedad: null, CO2: 26},
          {period: '2010 Q2', Temperatura: 27, Humedad: 94, CO2: 24},
          {period: '2010 Q3', Temperatura: 31, Humedad: 89, CO2: 25},
          {period: '2010 Q4', Temperatura: 28, Humedad: 87, CO2: 56},
          {period: '2011 Q1', Temperatura: 24, Humedad: 84, CO2: 22},
          {period: '2011 Q2', Temperatura: 26, Humedad: 93, CO2: 11},
          {period: '2011 Q3', Temperatura: 23, Humedad: 95, CO2: 15},
          {period: '2011 Q4', Temperatura: 22, Humedad: 87, CO2: 31},
          {period: '2012 Q1', Temperatura: 24, Humedad: 90, CO2: 20},
          {period: '2012 Q2', Temperatura: 20, Humedad: 93, CO2: 17}
        ],

          xkey: 'period',
          ykeys: ['Temperatura', 'Humedad', 'CO2'],
          labels: ['Temperatura', 'Humedad', 'Dioxido de Carbono'],
          hideHover: 'auto',
          lineWidth: 1,
          pointSize: 5,
          lineColors: ['#4a8bc2', '#ff6c60', '#a9d86e'],
          fillOpacity: 0.5,
          smooth: true
      });

      Morris.Bar({
        element: 'hero-bar',
        data: [
          {device: 'Temperatura', geekbench: 136},
          {device: 'Temperatura 3G', geekbench: 137},
          {device: 'Temperatura 3GS', geekbench: 275},
          {device: 'Temperatura 4', geekbench: 380},
          {device: 'Temperatura 4S', geekbench: 655},
          {device: 'Temperatura 5', geekbench: 1571}
        ],
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['Geekbench'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        barColors: ['#ac92ec']
      });

      new Morris.Line({
        element: 'examplefirst',
        xkey: 'year',
        ykeys: ['value'],
        labels: ['Value'],
        data: [
          {year: '2008', value: 20},
          {year: '2009', value: 10},
          {year: '2010', value: 5},
          {year: '2011', value: 5},
          {year: '2012', value: 20}
        ]
      });

      $('.code-example').each(function (index, el) {
        eval($(el).text());
      });
    });

}();




