package com.example.jenazad.pipo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_maps);
        //navigationView.setNavigationItemSelectedListener(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {

                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                // Inflate custom layout
                View v = getLayoutInflater().inflate(R.layout.marker_view, null);

                // Set desired height and width
                v.setLayoutParams(new RelativeLayout.LayoutParams(200, RelativeLayout.LayoutParams.WRAP_CONTENT));

                return v;
            }
        });

        mMap = googleMap;

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng biotech = new LatLng(-12.060058, -77.081750);
        LatLng rigel = new LatLng(-12.062430069307663,-77.08020955324173);
        LatLng lorena =   new LatLng(-12.062996633927044,-77.08261147141457);
        LatLng gabriel = new LatLng(-12.062498266964111,-77.08380907773972);
        LatLng geminis = new LatLng(-12.062878599730066,-77.08375543355942);
        LatLng farmarket = new LatLng(-12.06198940710931,-77.08388954401016);
        LatLng perufarma = new LatLng(-12.061396610390565,-77.07850635051727);
        /*Marker biotechmkr = mMap.addMarker(new MarkerOptions().position(biotech).title("BioTech Start Up Weekend"));
        Marker rigelmkr = mMap.addMarker(new MarkerOptions().position(rigel).title("Farmacia Rigel").snippet("Ranking: 1 \n nro estrellas: 4.2"));
        Marker lorenamkr = mMap.addMarker(new MarkerOptions().position(gabriel).title("Farmacia Gabriel").snippet("Ranking: 4 \n nro estrellas: 2.8"));
        Marker gabrielmkr = mMap.addMarker(new MarkerOptions().position(lorena).title("Farmacias Lorena").snippet("Ranking: 2 \n nro estrellas: 3.5"));
        Marker geminismkr = mMap.addMarker(new MarkerOptions().position(geminis).title("Farmacias Geminis").snippet("Ranking: 3 \n nro estrellas: 3.2"));
        Marker farmarketmkr = mMap.addMarker(new MarkerOptions().position(farmarket).title("Farmacias Farmarket").snippet("Ranking: 5 \n nro estrellas: 2.3"));
        biotechmkr.showInfoWindow();
        rigelmkr.showInfoWindow();
        gabrielmkr.showInfoWindow();
        lorenamkr.showInfoWindow();
        geminismkr.showInfoWindow();
        farmarketmkr.showInfoWindow();*/

        mMap.addMarker(new MarkerOptions().position(biotech).title("BioTech Start Up Weekend"));
        mMap.addMarker(new MarkerOptions().position(perufarma).title("Farmacia PeruFarma").snippet("Ranking: 1 \n nro estrellas: 4.5"));
        mMap.addMarker(new MarkerOptions().position(rigel).title("Farmacia Rigel").snippet("Ranking: 2 \n nro estrellas: 4.2"));
        mMap.addMarker(new MarkerOptions().position(gabriel).title("Farmacia Gabriel").snippet("Ranking: 5 \n nro estrellas: 2.8"));
        mMap.addMarker(new MarkerOptions().position(lorena).title("Farmacias Lorena").snippet("Ranking: 3 \n nro estrellas: 3.5"));
        mMap.addMarker(new MarkerOptions().position(geminis).title("Farmacias Geminis").snippet("Ranking: 4 \n nro estrellas: 3.2"));
        mMap.addMarker(new MarkerOptions().position(farmarket).title("Farmacias Farmarket").snippet("Ranking: 6 \n nro estrellas: 2.3"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(biotech));
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            //Intent i = new Intent(this, MapsActivity.class);
            //startActivity(i);
        }
        else if (id == R.id.nav_search) {
            Intent i = new Intent(this, MapsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_ranking) {
            Intent i = new Intent(this, RankingActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
